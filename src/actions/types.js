export const FETCH_CARD_REQUEST = 'fetch_card_request';
export const FETCH_CARD_COMPLETE = 'fetch_card_complete';
export const DISPLAY_ERROR = 'display_error';
