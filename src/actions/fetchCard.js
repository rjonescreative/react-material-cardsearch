import { FETCH_CARD_REQUEST } from './types.js';
import { FETCH_CARD_COMPLETE } from './types.js';
import { DISPLAY_ERROR } from './types.js';
import axios from 'axios';

const errors = [
  'By golly, I don\'t think that\'s a card',
  'We didn\'t find that one, but give it another go',
  'Either space monkeys are eating my face or we couldn\'t find that card',
  'That\'s not a card, bro',
  'Weird search, but ok',
  'I would say that was a good try, but it wasn\'t - search again',
  'That search is a big yikes from me, dawg'
];

const rand = () => {
  return Math.floor(Math.random() * errors.length);
}

export const fetchCard = searchName => async dispatch  => {
  dispatch({ type: FETCH_CARD_REQUEST });

  let response = {};
  try {
    response = await axios.get(`https://api.scryfall.com/cards/named/?fuzzy=${encodeURI(searchName)}`);
    dispatch({ type: FETCH_CARD_COMPLETE, payload: response.data });
  } catch (error) {
    console.log(error.response);
    dispatch({ type: FETCH_CARD_COMPLETE, payload: {} });
    dispatch({ type: DISPLAY_ERROR, payload: { error: error.response, message: errors[rand()], open: true } } );
  }
};
