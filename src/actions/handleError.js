import { DISPLAY_ERROR } from './types.js';
import axios from 'axios';

export const handleError = (error = '', message = '', open = false) => dispatch => {
  dispatch({ 
    type: DISPLAY_ERROR, 
    payload: { 
      error: error, 
      message: message, 
      open: open
    } 
  });
};
