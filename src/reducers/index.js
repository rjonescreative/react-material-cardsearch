import { combineReducers } from 'redux';
import card from './cardReducer';
import displayError from './displayError';

export default combineReducers({
  card,
  displayError
});
