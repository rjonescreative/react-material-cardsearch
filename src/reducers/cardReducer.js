import { FETCH_CARD_REQUEST } from '../actions/types';
import { FETCH_CARD_COMPLETE } from '../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_CARD_REQUEST:
      return { ...[], loading: true };
    case FETCH_CARD_COMPLETE:
      return { ...action.payload, loading: false };
    default:
      return state;
  };
};