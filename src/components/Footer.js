import React from 'react';

const Footer = () => {
  const footerStyle = {
    backgroundColor: 'lightblue',
    height: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }

  return (
    <div style={footerStyle}>
      <h5>Copyright Feathr 2018</h5>
    </div>
  );
}

export default Footer;
