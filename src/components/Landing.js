import React from 'react';
import { Link } from 'react-router-dom';
import Results from './Results';

class Landing extends React.Component {

  render() {
    const landingStyle = {
      margin: 0,
      padding: 25,
      lineHeight: 0,
      boxSizing: 'border-box'
    }

    return (
      <div style={landingStyle}>
        <Results />
      </div>
    )
  }
  
}
export default Landing;
