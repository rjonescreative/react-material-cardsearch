import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { handleError } from '../actions/handleError';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
  snackPosition: {
    top: 20,
  },
  snackStyle: {
    backgroundColor: theme.palette.error.dark
  }
});

const ErrorMessage = props => {
  const { classes } = props;
  const error = props.displayError;

  const handleClose = () => {
    props.handleError('', '', false);
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={error.open}
      onClose={handleClose}
      autoHideDuration={5000}
      className={classes.snackPosition}
      ContentProps={{
        'aria-describedby': 'message-id',
        'className': classes.snackStyle
      }}
      message={<div><div>{error.message}</div><div>({error.error && error.error.data.details || 'Error'})</div></div>}
    />
  )
}

const mapStateToProps = ({displayError}) => {
  return { displayError };
}

ErrorMessage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, { handleError }),
)(ErrorMessage);
