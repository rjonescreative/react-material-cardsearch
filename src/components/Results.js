import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
  paragraphSpacing: {
    marginBottom: 8,
  },
  card: {
    width: 350,
    textAlign: 'left',
  },
  media: {
    height: 180,
    backgroundPosition: '0 -10px',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  errors: {
    fontStyle: 'italic'
  },
  flavorText: {
    fontStyle: 'italic'
  },
  helpText: {
    fontStyle: 'italic',
    color: '#dedede',
    fontWeight: 300
  }
});

const Results = props => {
  const { classes } = props;
  const result = props.card;
  const loading = props.card.loading;
  const containerStyle = { width: '100%', marginTop: '25px', marginBottom: '25px', textAlign: 'center' };
  const resultStyle = { maxWidth: '250px' };

  const renderImage = () => {
    if (result.image_uris && result.image_uris.png) {
      return <img src={result.image_uris.png} alt={result.name} style={resultStyle} />
    }
  }

  const renderDivider = () => {
    return result.flavor_text ? <Divider className={classes.paragraphSpacing} /> : '';
  }

  const renderContent = () => {
    if (loading) {
      return (
        <CircularProgress className={classes.progress} />
      );
    } else if (result && result.error) {
      return (
        <div>
          <Typography variant="h6">
            
          </Typography>
          <Typography variant="body2" className={classes.errors}>
            ({result.error})
          </Typography>
        </div>
        
      )
    } else if (result && result.name) {
      return (
        <div>
          <Grid 
            container 
            spacing={24}
            alignItems="flex-start"
            justify="center"
          >
            <Grid item>
              {renderImage()}
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.media}
                  image={result.image_uris.art_crop}
                  title={result.name}
                />
                <CardContent>
                  <Typography className={classes.paragraphSpacing} variant="h5" component="h2">
                    {result.name || ''}
                  </Typography>
                  <Typography className={classes.paragraphSpacing} component="p">
                    {result.oracle_text || ''}
                  </Typography>
                  {renderDivider()}
                  <Typography component="p" className={classes.flavorText}>
                    {result.flavor_text || ''}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </div>
      )
    } else {
      return (
        <div>
          <Typography variant="h6">
            Let's search for a card
          </Typography>
          <Typography variant="body2" className={classes.helpText}>
            Examples: Condemn, Counterspell, Doom Blade, Shock, Giant Growth, Nicol Bolas, Sol Ring, Swamp
          </Typography>
        </div>
      )
    }
  }

  return (
    <div style={containerStyle}>
      { renderContent() }
    </div>
  )
}

const mapStateToProps = ({ card }) => {
  return { card }
}

Results.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps),
)(Results);
