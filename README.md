# react-material-cardsearch

> An example project using React, Redux, Material-UI, and Scryfall.com API.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
